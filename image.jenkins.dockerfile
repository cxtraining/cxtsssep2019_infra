FROM jenkinsci/blueocean:1.16.0

USER jenkins

ENV JENKINS_OPTS --prefix=/jenkins

COPY resources/jenkins-plugins.txt  /tmp

RUN /usr/local/bin/install-plugins.sh < /tmp/jenkins-plugins.txt

EXPOSE 8080