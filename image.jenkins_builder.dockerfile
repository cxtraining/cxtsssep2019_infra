FROM node:latest

RUN \
    \
    apt-get update; \
    apt-get install -y xvfb libgtk-3-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2; \
    apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common; \
    curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - ; \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"; \
    apt-get update; \
    apt-get install -y docker-ce docker-ce-cli containerd.io; \
    \
    \
    echo DONE;